# NodeHCNetSDK

#### 项目介绍
将海康sdk封装成Node插件

#### 软件架构
使用napi进行c++插件编写
当前日期2018-07
使用了当前最新node版本10.6.0中新增加的Asynchronous Thread-safe Function Calls系列api，该api均属于Stability: 1 - Experimental。
海康sdk使用CH-HCNetSDKV5.3.5.46_build20180518_Win64


#### 安装教程

1. 仅在windows下编译通过
2. vs使用的版本为2015.至少需要2013，node插件用2012及以前无法编译
3. 仅封装了视频接口
4. 安装的node版本至少是10.6.0

#### 使用说明

1. 使用命令node-gyp rebuild进行编译
2. js中引用生成的.node文件即可使用api接口
3. 海康的dll必须放在.node文件同一目录，同时不能漏了

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)